const express = require('express');
const path = require('path');
const exphbs = require('express-handlebars');
const app = express();

app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.engine('.hbs', exphbs({
    defaultLayout: 'main',
    layoutsDir: path.join(app.get('views'), 'layouts'),
    extname: '.hbs'
}));
app.set('view engine', '.hbs');

//Middlewares
app.use(express.urlencoded({extended: false}));

// Routes
app.use(require('./routes/index'));


// Static Files
app.use(express.static(path.join(__dirname, 'public')));
// Server is Listening
app.listen(app.get('port'), () => {
    console.log('server on port', app.get('port'));
});