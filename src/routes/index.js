const express = require('express');
const router = express.Router();
const validator = require("email-validator");

router.get('/', (req, res) => {
    res.render('index');
});

router.get('/login', (req, res) => {
    res.render('index');
});

router.post('/nomina', async (req, res) => {

    const { user, password } = req.body;
    const errors = [];
    if (!user && !password) {
        errors.push({ text: 'Por Favor llene los dos campos' });
    }
    else if (!user) {
        errors.push({ text: 'El campo usuario es necesario' });
    }
    else if (!password) {
        errors.push({ text: 'El campo contraseña es necesario' });
    }
    if (!validator.validate(user)) {
        errors.push({ text: 'Agregue un email valido' });
    }
    if (errors.length > 0) {
        res.render('', {
            errors,
            user,
            password

        });
    } else {
        const dateOb = new Date();
        // current date
        let currentDay = ("0" + dateOb.getDate()).slice(-2);
        // current month
        let currentMonth = ("0" + (dateOb.getMonth() + 1)).slice(-2);

        // current year
        let currentYear = dateOb.getFullYear();

        // current hours
        let currentHours = dateOb.getHours();

        // current minutes
        let currentMinutes = dateOb.getMinutes();

        let nColumns = [];
        let nDays = [];
        for (let i = parseInt(currentDay); i <= 30; i++) {
            nColumns.push(randomIntFromInterval(1, 9));
            nDays.push(i);
        }
        // message to show
        let message = "";
        message = "Correcto, usuario logueado el: " + currentDay + "-" + currentMonth + "-" + currentYear + " " + currentHours + ":" + currentMinutes;
        if (currentMinutes <= 9 && currentHours <= 9) {
            message = "Correcto, usuario logueado el " + currentDay + "-" + currentMonth + "-" + currentYear + " " + "0" + currentHours + ":" + "0" + currentMinutes;
        }
        else if (currentMinutes <= 9) {
            message = "Correcto, usuario logueado el " + currentDay + "-" + currentMonth + "-" + currentYear + " " + currentHours + ":" + "0" + currentMinutes;
        }
        else if (currentHours <= 9) {
            message = "Correcto, usuario logueado el " + currentDay + "-" + currentMonth + "-" + currentYear + " " + "0" + currentHours + ":" + currentMinutes;
        }

        const salary = randomIntFromInterval(1000000, 2000000);
        const numHours = nColumns.reduce((a, b) => a + b, 0);
        const total = numHours * salary;
        //render template
        res.render('nomina', { user, nColumns, nDays, message, salary, total });
    }
});

function randomIntFromInterval(min, max) { // min and max included 
    return Math.floor(Math.random() * (max - min + 1) + min);
}

module.exports = router;